#include "expr.hpp"
#include "cmdline.hpp"
#include <iostream>

using namespace std;
using namespace Commands;

void process_expr(ostream &os, expr initial_expr, vector<Command> const &commands) {
    expr e = move(initial_expr);
    for (const auto &cmd: commands) {
        expr e_init = e;
        cmd.match(
                [&](Derive const &derive) {
                    e = e_init->derive(derive.variable);
                },
                [&](Simplify const &) {
                    e = e_init->simplify();
                },
                [&](Evaluate const &evaluate) {
                    double result = e_init->evaluate(evaluate.variables);
                    os << result << endl;
                },
                [&](Print const &p) {
                    os << e_init;
                    os << endl;
                }
        );
    }
}

void handle_expr_line(std::ostream &os, std::string const &line, vector<Command> const &commands) {
    expr expression = create_expression_tree(line);
    try {
        process_expr(os, expression, commands);
    } catch (parse_error &exception) {
        os << "! " << exception.what() << endl;
    }
}
