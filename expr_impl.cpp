#include "expr_impl.hpp"

#include <iostream>
#include <cmath>
#include <limits>

#include <sstream>

namespace exprs {


    double expr_addition::evaluate(const variable_map_t &variables) const {
        return left_expr()->evaluate(variables)
               + right_expr()->evaluate(variables);
    }

    expr expr_addition::derive(std::string const &variable) const {
        expr f_d = left->derive(variable);
        expr g_d = right->derive(variable);
        //(f+g)' = f' + g'
        return f_d + g_d;
    }

    expr expr_addition::simplify() const {
        expr left_s = left_expr()->simplify();
        expr right_s = right_expr()->simplify();
        // x+0 = x
        if (right_s == expr::ZERO) {
            return left_s;
        }
        // 0+x = x
        if (left_s == expr::ZERO) {
            return right_s;
        }

        return left_s + right_s;
    }

    bool expr_addition::equals(const expr_base &b) const {
        return true;
    }

    double expr_subtraction::evaluate(const variable_map_t &variables) const {
        return left->shared_from_this()->evaluate(variables)
               - right_expr()->evaluate(variables);
    }

    expr expr_subtraction::derive(std::string const &variable) const {
        expr f_d = left->derive(variable);
        expr g_d = right->derive(variable);
        //(f-g)' = f' - g'
        return f_d - g_d;
    }

    expr expr_subtraction::simplify() const {
        expr left_s = left_expr()->simplify();
        expr right_s = right_expr()->simplify();
        // x-0 = x
        if (right_s == expr::ZERO) {
            return left_s;
        }

        return left_s - right_s;
    }

    bool expr_subtraction::equals(const expr_base &b) const {
        return true;
    }

    double expr_multiplication::evaluate(const variable_map_t &variables) const {
        return left_expr()->evaluate(variables)
               * right_expr()->evaluate(variables);
    }

    expr expr_multiplication::derive(std::string const &variable) const {
        expr f = left;
        expr g = right;
        expr f_d = f->derive(variable);
        expr g_d = g->derive(variable);
        //(f*g)' = f'*g + f*g'
        return (f_d * g) + (f * g_d);
    }

    expr expr_multiplication::simplify() const {
        expr left_s = left_expr()->simplify();
        expr right_s = right_expr()->simplify();
        // 0*x = 0
        // x*0 = 0
        if (left_s == expr::ZERO || right_s == expr::ZERO) {
            return expr::ZERO;
        }

        // 1*x = x
        if (left_s == expr::ONE) {
            return right_s;
        }

        // x*1 = x
        if (right_s == expr::ONE) {
            return left_s;
        }

        return left_s * right_s;
    }

    bool expr_multiplication::equals(const expr_base &b) const {
        return true;
    }

    double expr_division::evaluate(const variable_map_t &variables) const {
        return left_expr()->evaluate(variables)
               / right_expr()->evaluate(variables);
    }

    expr expr_division::derive(std::string const &variable) const {
        expr f = left;
        expr g = right;
        expr f_d = f->derive(variable);
        expr g_d = g->derive(variable);
        //(f/g)' = (f'*g - f*g')/g^2
        return ((f_d * g) - (f * g_d)) / (pow(g, expr::TWO));
    }

    expr expr_division::simplify() const {
        expr left_s = left_expr()->simplify();
        expr right_s = right_expr()->simplify();

        // 0/x = 0
        if (left_s == expr::ZERO && right_s->is_variable()) {
            return expr::ZERO;
        }

        // x/1 = x
        if (left_s->is_variable() && right_s == expr::ONE) {
            return left_s;
        }

        return left_s / right_s;
    }

    bool expr_division::equals(const expr_base &b) const {
        return true;
    }

    double expr_power::evaluate(const variable_map_t &variables) const {
        return pow(left_expr()->evaluate(variables),
                   right_expr()->evaluate(variables));
    }

    expr expr_power::derive(std::string const &variable) const {
        expr f = left;
        expr g = right;
        expr f_d = f->derive(variable);
        expr g_d = g->derive(variable);
        //(f^g)' = f^g * ( (f'*g)/f + log(f)*g' )
        return pow(f, g) * (((f_d * g) / f) + (log(f) * g_d));
    }

    expr expr_power::simplify() const {
        expr left_s = left_expr()->simplify();
        expr right_s = right_expr()->simplify();
        // 0^0 = 1 (not strictly true, but consistent with std::pow)
        // x^0 = 1
        if ((left_s == expr::ZERO && right_s == expr::ZERO) ||
            (left_s->is_variable() && right_s == expr::ZERO)) {
            return expr::ONE;
        }

        // x^1 = x
        if (left_s->is_variable() && right_s == expr::ZERO) {
            return left_s;
        }

        // 0^x = 0
        if (left_s == expr::ZERO && right_s->is_variable()) {
            return expr::ZERO;
        }

        return pow(left_s, right_s);
    }

    bool expr_power::equals(const expr_base &b) const {
        return true;
    }

    double expr_logarithm::evaluate(const variable_map_t &variables) const {
        double argument = single->evaluate(variables);
        if(argument <= 0) throw domain_exception("! Non-positive logarithm argument is not allowed.");
        return log(argument);
    }

    expr expr_logarithm::derive(std::string const &variable) const {
        expr f = single;
        expr f_d = f->derive(variable);
        // log(f)' = f' / f
        return f_d / f;
    }

    expr expr_logarithm::simplify() const {
        expr single_s = single_expr()->simplify();
        //log(1) = 0

        if (single_s == expr::ONE) {
            return expr::ZERO;
        }

        return make_shared<expr_logarithm>(expr_logarithm(single_s));
    }

    bool expr_logarithm::equals(const expr_base &b) const {
        return true;
    }

    double expr_sinus::evaluate(const variable_map_t &variables) const {
        return sin(single->shared_from_this()->evaluate(variables));
    }

    expr expr_sinus::derive(std::string const &variable) const {
        expr f = single;
        expr f_d = f->derive(variable);
        // sin(f)' = cos(f) * f'
        return cos(f) * f_d;
    }

    expr expr_sinus::simplify() const {
        return make_shared<expr_sinus>(expr_sinus(single_expr()->simplify()));
    }

    bool expr_sinus::equals(const expr_base &b) const {
        return true;
    }

    double expr_cosinus::evaluate(const variable_map_t &variables) const {
        return cos(single->shared_from_this()->evaluate(variables));
    }

    expr expr_cosinus::derive(std::string const &variable) const {
        expr f = single;
        expr f_d = f->derive(variable);
        //cos(f)' = (0-sin(f)) * f'
        return (expr::ZERO - sin(f)) * f_d;
    }

    expr expr_cosinus::simplify() const {
        return make_shared<expr_cosinus>(expr_cosinus(single_expr()->simplify()));
    }

    bool expr_cosinus::equals(const expr_base &b) const {
        return true;
    }

    double expr_number::evaluate(const variable_map_t &variables) const {
        return number;
    }

    expr expr_number::derive(std::string const &variable) const {
        return expr::ZERO;
    }

    expr expr_number::simplify() const {
        return shared_from_this();
    }

    void expr_number::write(std::ostream &out, WriteFormat fmt) const {
        out << number;
    }

    bool expr_number::equals(const expr_base &b) const {
        return true;
    }

    double expr_variable::evaluate(const variable_map_t &variables) const {
        if (variables.count(variable_name)) {
            return variables.at(variable_name);
        } else {
            stringstream ss;
            ss << "Evaluation error: variable of name " << variable_name
               << " not found in variable map.";
            throw  unbound_variable_exception(ss.str());
        }
    }

    expr expr_variable::derive(std::string const &variable) const {
        if (variable == variable_name) {
            return expr::ONE;
        } else return expr::ZERO;
    }

    expr expr_variable::simplify() const {
        return shared_from_this();
    }

    bool expr_variable::equals(const expr_base &b) const {
        throw std::logic_error("not implemented yet");
    }

    void expr_variable::write(std::ostream &out, WriteFormat fmt) const {
        out << variable_name;
    }


    void binary_expr::write(std::ostream &out, WriteFormat fmt) const {
        expr left_e = left_expr();
        expr right_e = right_expr();
        out << "(";
        out << getOperator();
        out << " ";
        out << left_e;
        out << " ";
        out << right_e;
        out << ")";
    }

    bool binary_expr::equals(const expr_base &b) const {
        return true;
    }

    double unary_expr::evaluate(const variable_map_t &variables) const {
        throw std::logic_error("not implemented yet");
    }


    void unary_expr::write(std::ostream &out, WriteFormat fmt) const {
        expr single_e = single->shared_from_this();
        out << "(";
        out << getFunction();
        out << " ";
        out << single_e;
        out << ")";
    }

    bool unary_expr::equals(const expr_base &b) const {
        return true;
    }

} // namespace exprs
