#include "expr.hpp"
#include "expr_impl.hpp"
#include "tokenizer.hpp"

#include <stdexcept>
#include <vector>
#include <sstream>

#include <stack>

const expr expr::ZERO = expr::number(0.0);
const expr expr::ONE = expr::number(1.0);
const expr expr::TWO = expr::number(2.0);

using namespace std;
using namespace exprs;

expr expr::number(double n) {
    expr_number number(n);
    return make_shared<expr_number>(number);
}

expr expr::variable(string name) {
    expr_variable variable(name);
    return make_shared<expr_variable>(variable);
}

expr operator+(expr a, expr b) {
    expr_addition e(a.ptr, b.ptr);
    return make_shared<expr_addition>(e);
}

expr operator-(expr a, expr b) {
    expr_subtraction e(a.ptr, b.ptr);
    return make_shared<expr_subtraction>(e);
}

expr operator*(expr a, expr b) {
    expr_multiplication e(a.ptr, b.ptr);
    return make_shared<expr_multiplication>(e);
}

expr operator/(expr a, expr b) {
    expr_division e(a.ptr, b.ptr);
    return make_shared<expr_division>(e);
}

expr pow(expr m, expr e) {
    expr_power power(m, e);
    return make_shared<expr_power>(power);
}

expr sin(expr e) {
    expr_sinus sin(e);
    return make_shared<expr_sinus>(sin);
}

expr cos(expr e) {
    expr_cosinus cos(e);
    return make_shared<expr_cosinus>(cos);
}

expr log(expr e) {
    expr_logarithm log(e);
    return make_shared<expr_logarithm>(log);
}

void addExprToStack(stack<expr> &outputStack, const Token &token) {
    expr newExpr;
    if (outputStack.empty()) {
        throw parse_error("! Operator or token require at least one argument.");
    }
    expr single = outputStack.top();
    outputStack.pop();

    if (token.is_function()) {
        if (token.id == TokenId::Identifier) { //unary functions
            if (token.identifier == "sin") newExpr = sin(single);
            if (token.identifier == "cos") newExpr = cos(single);
            if (token.identifier == "log") newExpr = log(single);
        } else { //binary functions
            if (outputStack.empty()) {
                throw parse_error("! Operator requires two operands.");
            }
            expr second = outputStack.top();
            outputStack.pop();

            swap(single, second);

            if (token.id == TokenId::Plus) newExpr = single + second;
            if (token.id == TokenId::Minus) newExpr = single - second;
            if (token.id == TokenId::Multiply) newExpr = single * second;
            if (token.id == TokenId::Divide) newExpr = single / second;
            if (token.id == TokenId::Power) newExpr = pow(single, second);
        }
    } else if (token.id == TokenId::Number) {
        newExpr = expr::number(token.number);
    } else if (token.id == TokenId::Identifier) {
        newExpr = expr::variable(token.identifier);
    } else if (token.id == TokenId::LParen || token.id == TokenId::RParen) {
        throw logic_error("There should not be parentheses!");
    } else throw logic_error("There is no other cases.");
    outputStack.push(newExpr);
}

void process_token(Token &token, stack<expr> &outputStack, stack<Token> &operatorStack) {
    switch (token.id) {
        case TokenId::Number: {
            outputStack.push(expr::number(token.number));
        }
            break;

        case TokenId::LParen: {
            operatorStack.push(token);
        }
            break;
        case TokenId::RParen: {
            if (operatorStack.empty()) {
                throw parse_error("! Right parentheses without left");
            }
            Token t;

            while ((t = operatorStack.top()).id != TokenId::LParen) {
                addExprToStack(outputStack, t); //operatorStack.top to outputStack!
                operatorStack.pop();
                if (operatorStack.empty()) {
                    throw parse_error("! Right parentheses without left.");
                }
            }

            //pop left parentheses
            if (operatorStack.top().id != TokenId::LParen)
                throw logic_error("There should be left parentheses!");

            operatorStack.pop();

            if (!operatorStack.empty() &&
                (t = operatorStack.top()).is_function() &&
                t.id == TokenId::Identifier) { //nebo to plati i pro operator?
                addExprToStack(outputStack, t);
                operatorStack.pop();
            }
        }
            break;
        case TokenId::Identifier: {
            if (token.is_function()) {
                operatorStack.push(token);
            } else { //variable
                outputStack.push(expr::variable(token.identifier));
            }
        }
            break;
        default: //operator
        {
            const Token &o1 = token;
            Token o2;

            while (!operatorStack.empty()
                   && (o2 = operatorStack.top()).id != TokenId::LParen
                   && (o2.op_precedence() > o1.op_precedence()
                       || (o2.op_precedence() == o1.op_precedence()
                           && o1.associativity() == Associativity::Left))) {
                operatorStack.pop();
                addExprToStack(outputStack, o2);
            }
            operatorStack.push(o1);
        }
    }
}

expr create_expression_tree(const string &expression) {
    expr e{};

    stack<expr> outputStack;
    stack<Token> operatorStack;
    stringstream ss(expression);
    Tokenizer t(ss);
    Token token = t.next();
    do {
        process_token(token, outputStack, operatorStack);
        token = t.next();
    } while (token.id != TokenId::End);

    while (!operatorStack.empty()) {
        token = operatorStack.top();
        operatorStack.pop();
        if (token.id == TokenId::LParen) {
            throw parse_error("! Left parentheses without right.");
        }
        addExprToStack(outputStack, token);
    }

    if (outputStack.size() == 0) {
        throw parse_error("! There is nothing.");
    } else if (outputStack.size() > 1) {
        throw parse_error("! Not everything from outputStack was processed.");
    }

    return outputStack.top();
}

bool operator==(const expr &a, const expr &b) {
    stringstream ss_a, ss_b;
    ss_a << a;
    ss_b << b;
    return ss_a.str() == ss_b.str();
}

ostream &operator<<(ostream &os, const expr &e) {
    e->write(os, expr_base::WriteFormat::Prefix);
}

ostream &operator<<(ostream &os, const fmt_expr &fmt) {
    fmt.e->write(os, fmt.fmt);
}
