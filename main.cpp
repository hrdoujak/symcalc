#include "cmdline.hpp" // parse_command
#include "app.hpp" // handle_expr_line

#include <vector>
#include <iostream>

using namespace Commands;
using namespace std;

int main(int argc, char *argv[]) {
    vector<Command> commands(0);
    for (int i = 1; i < argc; i++) {
        try{ commands.push_back(parse_command(argv[i])); }
        catch(runtime_error & error) { cout << error.what() << endl;}
    }
    string line;
    while (getline(cin, line)) {
        handle_expr_line(cout, line, commands);
    }
}
