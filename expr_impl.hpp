#pragma once

#include "expr.hpp"
#include <iosfwd>

using namespace std;

namespace exprs {

    class expr_number : public expr_base {
    public:
        explicit expr_number(double n) : number(n) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

    private:
        double number;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        bool is_number() const override { return true; };
    };

    class expr_variable : public expr_base {
    public:
        explicit expr_variable(const string &variableName) : variable_name(variableName) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        const string variable_name;
    private:

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        bool is_variable() const override { return true; };
    };

    class binary_expr : public expr_base {
    public:
        binary_expr(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                expr_base(l, r) {}

    protected:
        expr left_expr() const {
            return left->shared_from_this();
        }

        expr right_expr() const {
            return right->shared_from_this();
        }

    private:
        virtual string getOperator() const = 0;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;
    };

    class unary_expr : public expr_base {
    public:
        explicit unary_expr(shared_ptr<const expr_base> single) :
                expr_base(single) {}

        double evaluate(const variable_map_t &variables) const override;

    protected:
        expr single_expr() const {
            return single->shared_from_this();
        }

    private:
        virtual string getFunction() const = 0;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;
    };

    class expr_addition : public binary_expr {
    public:
        expr_addition(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                binary_expr(l, r) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        string getOperator() const override { return "+"; }
    private:

        bool equals(const expr_base &b) const override;

    };

    class expr_subtraction : public binary_expr {
    public:
        expr_subtraction(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                binary_expr(l, r) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        string getOperator() const override { return "-"; }
    private:

        bool equals(const expr_base &b) const override;
    };

    class expr_multiplication : public binary_expr {
    public:
        expr_multiplication(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                binary_expr(l, r) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        string getOperator() const override { return "*"; }
    private:

        bool equals(const expr_base &b) const override;
    };

    class expr_division : public binary_expr {
    public:
        expr_division(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                binary_expr(l, r) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        string getOperator() const override { return "/"; }
    private:

        bool equals(const expr_base &b) const override;
    };

    class expr_power : public binary_expr {
    public:
        expr_power(shared_ptr<const expr_base> l, shared_ptr<const expr_base> r) :
                binary_expr(l, r) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

    private:
        string getOperator() const override { return "^"; }

        bool equals(const expr_base &b) const override;
    };

    class expr_logarithm : public unary_expr {
    public:
        expr_logarithm(shared_ptr<const expr_base> arg) :
                unary_expr(arg) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

    private:
        string getFunction() const override { return "log"; }

        bool equals(const expr_base &b) const override;
    };


    class expr_sinus : public unary_expr {
    public:
        expr_sinus(shared_ptr<const expr_base> arg) :
                unary_expr(arg) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

    private:

        string getFunction() const override { return "sin"; }

        bool equals(const expr_base &b) const override;
    };

    class expr_cosinus : public unary_expr {
    public:
        expr_cosinus(shared_ptr<const expr_base> arg) :
                unary_expr(arg) {}

        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

    private:

        string getFunction() const override { return "cos"; }

        bool equals(const expr_base &b) const override;
    };
}
